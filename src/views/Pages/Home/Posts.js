import React from "react";
import {
    Row,
    Col,
    InputGroup,
    Input,
    Button,
    Card,
    CardBody,
    CardHeader,
    CardFooter,
    CardImg,
    Modal, ModalHeader, ModalBody, Form,
} from "reactstrap";
import './Home.css'
import Map from './../Map';
import SplitterLayout from "../components/SplitLayout/SplitterLayout";
import mapapi from "./../../../api/map.api"
import houseApi from "./../../../api/house.api"
import apartmentApi from "./../../../api/appartment.api"
import Carousel from 'react-elastic-carousel'
import PerfectScrollbar from 'react-perfect-scrollbar';
import ReactPaginate from 'react-paginate';
import PostDetail from "./PostDetail";
var places = require('places.js');
const TOKEN = "803840e9bacbc2";
// import 'react-perfect-scrollbar/dist/css/styles.css';

class Posts extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            _isMounted: false,
            homeType: 'sale_apartment',
            changeRegion: false,
            pDepartment: [],
            listDepartment: [],
            pHouse: [],
            listHouse: [],
            pageHouse: 1,
            pageDepartment: 1,
            pageLimit: 10,
            city_id: '',
            district_id: '',
            ward_id: '',
            listPost: [],//danh sách bài viết
            pageCount: 0,
            forcePage: 0,
            forcePageDepartMent: 0,
            forcePageHouse: 0,
            pageCountHouse: 0,
            pageCountDepartment: 0
        };
        this.mapRef = React.createRef();
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    componentDidMount() {
        let me = this;
        me.setState(state => ({
            _isMounted: !state._isMounted
        }))
    }
    // componentWillUpdate(e) {
    //     if (e !== this.props) {
    //         this.loadPosts(e.typeBds, e.idLocation);
    //     }
    // }
    // loadPosts = (typeBds, idLocation) => {
    //     if (typeBds === 'house') {
    //         this.getHouseByCity(idLocation);
    //         this.setState({ valueTypeHome: 'sale_house' });

    //     }
    //     else {
    //         this.getApartmentByCity(idLocation);
    //         this.setState({ valueTypeHome: 'sale_apartment' });
    //     }

    // }
    // search(e) {
    //     e.preventDefault();
    //     let textSearch = this.state.textSearch;
    //     let me = this;
    //     if (textSearch.length > 2) {
    //         mapapi.algoliaGeocode(textSearch).then(function (result) {
    //             let loc = result._geoloc;
    //             if (me.mapRef.current) {
    //                 me.mapRef.current.onLocationChange(loc);
    //             }
    //         })
    //     }
    // }

    openDialogBaoGia = () => {
        if (this.state._isMounted) {
            this.setState({ modal: !this.state.modal })
        }
    }

    chooseHome(value) {
        let me = this;
        me.setState({ homeType: value });
        if (value === 'sale_apartment') {
            if (this.state.listDepartment.length === 0) {
                this.checkUnit(value);
            }
            else {
                if (this.state.changeRegion === false) {
                    me.setState({
                        listPost: me.state.listDepartment,
                        forcePage: me.state.forcePageDepartMent,
                        pageCount: me.state.pageCountDepartment
                    });
                } else this.checkUnit(value);
            }
        }
        else {
            if (this.state.listHouse.length === 0) {
                this.checkUnit(value);
            } else {
                if (this.state.changeRegion === false) {
                    me.setState({
                        listPost: me.state.listHouse,
                        forcePage: me.state.forcePageHouse,
                        pageCount: me.state.pageCountHouse
                    });
                }
                else this.checkUnit(value);
            }
        }
    }

    onCityChange = (obj) => {
        let me = this;
        if (this.state.city_id !== obj.id) {
            this.setState({ city_id: obj.id, unitChange: 'city', changeRegion: true })
            if (me.state.homeType === 'sale_apartment')
                me.getApartmentByCity(obj.id);
            else
                me.getHouseByCity(obj.id);
        }
        else {
            if (me.state.homeType === 'sale_apartment') {
                if (this.state.listDepartment.length === 0) {
                    this.checkUnit('sale_apartment');
                }
                else {
                    if (this.state.changeRegion === false) {
                        me.setState({
                            changeRegion: false,
                            listPost: me.state.listDepartment,
                            forcePage: me.state.forcePageDepartMent,
                            pageCount: me.state.pageCountDepartment
                        });
                    }
                    else this.checkUnit('sale_apartment');
                }
            }
            else {
                if (this.state.listHouse.length === 0) {
                    this.checkUnit('sale_house');
                } else {
                    if (this.state.changeRegion === false) {
                        me.setState({
                            changeRegion: false,
                            listPost: me.state.listHouse,
                            forcePage: me.state.forcePageHouse,
                            pageCount: me.state.pageCountHouse
                        });
                    }
                    else this.checkUnit('sale_house');
                }
            }
        }
    }

    onDistrictChange = (obj) => {
        let me = this;
        if (this.state.district_id !== obj.id) {
            this.setState({ district_id: obj.id, unitChange: 'district', changeRegion: true })
            if (me.state.homeType === 'sale_apartment')
                me.getApartmentByDistrict(obj.id);
            else
                me.getHouseByDistrict(obj.id);
        } else {
            if (me.state.homeType === 'sale_apartment') {
                me.setState({
                    listPost: me.state.listDepartment,
                    forcePage: me.state.forcePageDepartMent,
                    pageCount: me.state.pageCountDepartment
                });
            }
            else {
                me.setState({
                    listPost: me.state.listHouse,
                    forcePage: me.state.forcePageHouse,
                    pageCount: me.state.pageCountHouse
                });
            }
        }
    }

    onWardChange = (obj) => {
        let me = this;
        if (this.state.ward_id !== obj.id) {
            this.setState({ ward_id: obj.id, unitChange: 'ward', changeRegion: true })
            if (me.state.homeType === 'sale_apartment')
                me.getApartmentByWard(obj.id);
            else
                me.getHouseByWard(obj.id);
        } else {
            if (me.state.homeType === 'sale_apartment') {
                me.setState({
                    listPost: me.state.listDepartment,
                    forcePage: me.state.forcePageDepartMent,
                    pageCount: me.state.pageCountDepartment
                });
            }
            else {
                me.setState({
                    listPost: me.state.listHouse,
                    forcePage: me.state.forcePageHouse,
                    pageCount: me.state.pageCountHouse
                });
            }
        }

    }

    receivePrice = (obj) => {
        this.setState({ itemSelected: obj })
        let arrImages = obj.images.toString().split(",");
        this.setState({ listImageItemSelected: arrImages })
        this.openDialogBaoGia();
    }

    getHouseByCity = (id) => {
        let me = this;
        if (this.state.unitChange !== "city") return false;
        houseApi.getByCity(id, this.state.pageHouse, this.state.pageLimit).then(res => {
            if (res && res.data && res.data.length) {
                let numOfPage = Math.ceil(Number(res.total_pages) / me.state.pageLimit);
                this.setState({
                    pHouse: res.data,
                    listHouse: res.data,
                    listPost: res.data,
                    pageCount: numOfPage,
                    pageCountHouse: numOfPage
                })
            }
        }).catch(error => {
            console.log(error);
        })
    }
    getHouseByDistrict = (id) => {
        let self = this;
        if (this.state.unitChange !== "district") return false;
        houseApi.getByDistrict(id, this.state.pageHouse, this.state.pageLimit).then(res => {
            if (res && res.data && res.data.length) {
                let numOfPage = Math.ceil(Number(res.total_pages) / self.state.pageLimit);
                self.setState({
                    listHouse: res.data,
                    listPost: res.data,
                    pageCount: numOfPage,
                    pageCountHouse: numOfPage
                })
            }
        }).catch(error => {
            console.log(error);
        })
    }

    getHouseByWard = (id) => {
        let self = this;
        if (this.state.unitChange !== "ward") return false;
        houseApi.getByWard(id, this.state.pageHouse, self.state.pageLimit).then(res => {
            if (res && res.data && res.data.length) {
                let numOfPage = Math.ceil(Number(res.total_pages) / self.state.pageLimit);
                self.setState({
                    listHouse: res.data,
                    listPost: res.data,
                    pageCount: numOfPage,
                    pageCountHouse: numOfPage
                })
            }
        }).catch(error => {
            console.log(error);
        })
    }

    getApartmentByCity = (id) => {
        let self = this;
        if (this.state.unitChange !== "city") return false;
        apartmentApi.getByCity(id, this.state.pageDepartment, self.state.pageLimit).then(res => {
            if (res && res.data && res.data.length) {
                let numOfPage = Math.ceil(Number(res.total_pages) / self.state.pageLimit);
                self.setState({
                    pDepartment: res.data,
                    listDepartment: res.data,
                    listPost: res.data,
                    pageCount: numOfPage,
                    pageCountDepartment: numOfPage
                })
            }
        }).catch(error => {
            console.log('co loi xay ra');
            console.log(error);
        })
    }

    getApartmentByDistrict = (id) => {
        let self = this;
        if (this.state.unitChange !== "district") return false;
        apartmentApi.getByDistrict(id, this.state.pageDepartment, this.state.pageLimit).then(res => {
            if (res && res.data && res.data.length) {
                let numOfPage = Math.ceil(Number(res.total_pages) / self.state.pageLimit);
                self.setState({
                    listDepartment: res.data,
                    listPost: res.data,
                    pageCount: numOfPage,
                    pageCountDepartment: numOfPage
                })
            }
        }).catch(error => {
            console.log('loi xayi ra');
            console.log(error);
        })
    }

    getApartmentByWard = (id) => {
        let self = this;
        if (this.state.unitChange !== "ward") return false;
        apartmentApi.getByWard(id, this.state.pageDepartment, self.state.pageLimit).then(res => {
            if (res && res.data && res.data.length) {
                let numOfPage = Math.ceil(Number(res.total_pages) / self.state.pageLimit);
                self.setState({
                    listDepartment: res.data,
                    listPost: res.data,
                    pageCount: numOfPage,
                    pageCountDepartment: numOfPage
                })
            }
        }).catch(error => {
            console.log(error);
        })
    }

    locatePoint = (item) => {
        if (this.props.mapRef.current) {
            var loc = {
                lng: item.longitude,
                lat: item.latitude
            }
            this.props.mapRef.current.onLocatePoint(loc);
        }
    }

    handlePageChange(statePage, event) {
        if (this.state.homeType === "sale_apartment") {
            this.setState({
                pageDepartment: Number(statePage.selected) + 1,
                forcePageDepartMent: Number(statePage.selected),
                forcePage: Number(statePage.selected),
            }, () => {
                this.checkUnit()
            })
        } else {
            this.setState({
                pageHouse: Number(statePage.selected) + 1,
                forcePage: Number(statePage.selected),
                forcePageHouse: Number(statePage.selected),
            }, () => {
                this.checkUnit()
            })
        }
    }

    checkUnit = (type) => {
        if (type === "sale_apartment") {
            if (this.state.unitChange === "city") this.getApartmentByCity(this.state.city_id);
            if (this.state.unitChange === "district") this.getApartmentByDistrict(this.state.district_id);
            if (this.state.unitChange === "ward") this.getApartmentByWard(this.state.ward_id);
        } else {
            if (this.state.unitChange === "city") this.getHouseByCity(this.state.city_id);
            if (this.state.unitChange === "district") this.getHouseByDistrict(this.state.district_id);
            if (this.state.unitChange === "ward") this.getHouseByWard(this.state.ward_id);
        }

    }

    render() {
        const gridlayout1 = { xs: 12, sm: 12, md: 12, lg: 3, xl: 3, };
        const gridlayout2 = { xs: 12, sm: 12, md: 12, lg: 6, xl: 6 };
        const gridlayout3 = { xs: 12, sm: 12, md: 12, lg: 3, xl: 3 };
        const externalCloseBtn = <button className="close" style={{ position: 'absolute', top: '15px', right: '15px' }} onClick={this.openDialogBaoGia}>&times;</button>;
        const isView = this.state.pageCount > 1;
        return (
            <div>
                <Modal size="lg" isOpen={this.state.modal} toggle={this.openDialogBaoGia} external={externalCloseBtn}>
                    <ModalBody>
                        <PostDetail post={this.state.itemSelected} images={this.state.listImageItemSelected} ></PostDetail>
                    </ModalBody>
                </Modal>
                <Card style={{ height: "55vh" }}>
                    <PerfectScrollbar>
                        <CardBody >
                            {
                                this.state.listPost.map((item, index) => (
                                    <div key={index} onClick={() => this.locatePoint(item)}>
                                        <Row key={index} className="rowHome">
                                            <Col {...gridlayout1}>
                                                <Card inverse>
                                                    <CardImg width="100%" src={item.images.toString().split(",")[0]} alt="ảnh minh hoạ" />
                                                </Card>
                                            </Col>
                                            <Col {...gridlayout2}>
                                                <p>
                                                    {item.title}
                                                </p>
                                                <Button color="primary" size="sm" onClick={(event) => this.receivePrice(item)}>Thêm thông tin</Button>
                                            </Col>
                                            <Col {...gridlayout3}>
                                                <p>{item.address}</p>
                                            </Col>
                                        </Row>
                                    </div>
                                ))
                            }
                        </CardBody>
                    </PerfectScrollbar>
                    <CardFooter style={{ padding: " 0", float: "right", height: "34px" }}>
                        <div id="react-paginate" style={{ paddingTop: "0px" }} className={isView ? "" : "d-none"} >
                            <ReactPaginate
                                previousLabel={'Trước'}
                                nextLabel={'Tiếp'}
                                pageCount={this.state.pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={3}
                                onPageChange={this.handlePageChange}
                                containerClassName={'pagination'}
                                subContainerClassName={'pages pagination'}
                                activeClassName={'active'}
                                forcePage={this.state.forcePage}
                            />
                        </div>
                    </CardFooter>
                </Card>
            </div >
        );
    }
}
export default Posts;
