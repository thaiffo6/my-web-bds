import React from "react";
import {
    Row,
    Col,
    Input,
    Button,
    Form,
} from "reactstrap";
import './Home.css'
import Carousel from 'react-elastic-carousel';
import PerfectScrollbar from 'react-perfect-scrollbar';
class PostDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            postInfo: this.props.post,
            images: this.props.images,
            _isMounted: false,
            valueTypeHome: "sale_apartment",
            listImagepost: [],
        };
    }

    componentDidMount() {

    }

    render() {
        return (
            <div >
                <div>
                    <Row >
                        <Carousel itemsToScroll={1} itemsToShow={4} >
                            {this.state.images.map((imgUrl, index) =>
                                <img key={index} alt="anh" src={imgUrl} height="150" width="200" />
                            )}
                        </Carousel>
                    </Row>
                </div>
                <PerfectScrollbar>
                    <div style={{ overflowX: "hidden" }}>
                        <Row>
                            <Col sm="7">
                                <div>
                                    <p>
                                        {this.state.postInfo.description}
                                    </p>
                                    <p>
                                        Diện tích: {this.state.postInfo.size}.<br />
                                    Giá:      {this.state.postInfo.price}.<br />
                                    Ngày bắt đầu: {this.state.postInfo.start_date}.<br />
                                    Ngày kết thúc: {this.state.postInfo.start_date}.<br />
                                    Liên hệ: {this.state.postInfo.contact_mobile}.
                                </p>
                                </div>
                                <div>
                                    <div style={{ marginBottom: "5px", fontWeight: "bold" }}>Dự toán giá</div>
                                    <div className="css-14uk5gm">
                                        <div className="css-3epm7k">
                                            <div style={{ width: "25%" }} className="css-1n5rwcf"></div>
                                            <div style={{ width: "25%" }} className="css-151ox5u"></div>
                                            <div style={{ width: "50%" }} className="css-1b6sh8v"></div>
                                            <div className="css-2hpe9w"></div>
                                            <div className="css-qzfvxb">
                                                <div className="css-1trmj4g">
                                                    <div className="css-15s1vqh">{"cao nhất: " + this.state.postInfo.over2.toFixed(2)}</div>
                                                </div>
                                                <div className="css-1hp2rub"></div>
                                            </div>
                                        </div>
                                        <div className="css-17os1zp">
                                            <div style={{ position: "absolute", left: "5%" }} className="css-15s1vqh">{this.state.postInfo.under2.toFixed(2)}</div>
                                            <div style={{ position: "absolute", left: "25%" }} className="css-15s1vqh">{this.state.postInfo.under1.toFixed(2)}</div>
                                            <div style={{ position: "absolute", left: "50%" }} className="css-1iww29j">
                                                <div className="css-1ovez44">Trung bình</div>
                                                <div className="css-1ovez44">{this.state.postInfo.mean.toFixed(2)}</div>
                                            </div>
                                            <div style={{ position: "absolute", left: "75%" }} className="css-15s1vqh">{this.state.postInfo.over1.toFixed(2)}</div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col sm="5">
                                <p>
                                    <strong>Ước tính tài sản của bạn</strong><br />
                                </p>
                                <Form>
                                    <Row form style={{ marginBottom: "5px" }}>
                                        <Col >
                                            <Input type="text" name="hoten" id="hoten" placeholder="Họ tên" />
                                        </Col>
                                    </Row>
                                    <Row form style={{ marginBottom: "5px" }}>
                                        <Col >
                                            <Input type="number" name="dienthoai" id="dienthoai" placeholder="Điện thoại" />
                                        </Col>
                                    </Row>
                                    <Row form style={{ marginBottom: "5px" }}>
                                        <Col >
                                            <Input type="email" name="email" id="email" placeholder="Email" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col >
                                            <Button style={{ width: "100%" }} color="info">Gửi</Button>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <p>
                                                Bằng cách nhấp vào vào <strong>Gửi</strong>, bạn chấp nhận các điều kiện sử dụng chung và
                                            chính sách bảo mật của chúng tôi . MeilleurAgents xử lý dữ liệu cần thiết cho việc sử dụng
                                            các dịch vụ của nó và dành cho các chuyên gia và MeillAgents liên hệ chuyên nghiệp.
                                            Bạn có quyền truy cập, cải chính, xóa và phản đối, đối với thông tin liên quan đến bạn.
                                            Để thực hiện quyền của bạn, liên hệ với chúng tôi .
                                            </p>
                                        </Col>
                                    </Row>
                                </Form>
                            </Col>

                        </Row>
                    </div></PerfectScrollbar>
            </div>
        );
    }
}
export default PostDetail;
