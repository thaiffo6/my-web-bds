import React from "react";
import {
    Row,
    Col,
    InputGroup,
    Input,
    Nav, NavItem, NavLink,
    TabContent, TabPane,
    Button,
    Card,
    CardBody,
    CardHeader,
    CardFooter,
    CardImg,
    Modal, ModalHeader, ModalBody, Form,
} from "reactstrap";
import classNames from 'classnames';
import './Home.css'
import Map from './../Map';
import SplitterLayout from "../components/SplitLayout/SplitterLayout";
import mapapi from "./../../../api/map.api"
import houseApi from "./../../../api/house.api"
import apartmentApi from "./../../../api/appartment.api"
import Carousel from 'react-elastic-carousel'
import PerfectScrollbar from 'react-perfect-scrollbar';
import ReactPaginate from 'react-paginate';
import Posts from "./Posts";
import Price from "./Price";
import PostDetail from "./PostDetail";
var places = require('places.js');
const TOKEN = "803840e9bacbc2";
class TrangChu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            _isMounted: false,
            textSearch: '',
            loc: null,
            modal: false,
            toggle: false,
            textHome: "Chung cư",
            valueTypeHome: "apartment",
            cityInfo: {},
            pDepartment: [],
            listDepartment: [],
            pHouse: [],
            listHouse: [],
            itemSelected: {},
            listImageItemSelected: [],
            pageHouse: 1,
            pageDepartment: 1,
            pageLimit: 10,
            city_id: "",
            district_id: "",
            ward_id: "",
            listPost: [],//danh sách bài viết
            postId: "",//id bài viết
            tabIndex: '0',//tab index để switch giữa bài viết và giá 0- bài viết, 1 là giá
            unitChange: "city",
            hasMore: true,
            firstMapClick: true,
            totalMeanCC: "0 VNĐ",
            fromValueCC: "0 VNĐ",
            toValueCC: "0 VNĐ",
            totalMeanND: "0 VNĐ",
            fromValueND: "0 VNĐ",
            toValueND: "0 VNĐ",
            address: "",
            showColLeft: false,
            forcePageDepartMent: 0,
            forcePageHouse: 0,
            pageCountHouse: 0,
            pageCountDepartment: 0
        };
        this.mapRef = React.createRef();
        this.postRef = React.createRef();
        this.priceRef = React.createRef();
    }

    componentDidMount() {
        let me = this;
        var placesAutocomplete = places({
            appId: 'pl0K4C57LHC4',
            apiKey: '74d8852573f9c6ff69b6e9e87f05efb6',
            container: document.querySelector('#address-input')
        });
        placesAutocomplete.configure({
            language: 'vn',
            countries: ['vn'],
        });
        placesAutocomplete.on('change', e => {
            if (me.mapRef.current)
                me.mapRef.current.onLocatePoint(e.suggestion.latlng);
        });
        placesAutocomplete.on('clear', function () {
            if (me.mapRef.current)
                me.mapRef.current.clearAdmin();
        })
        fetch('https://ipinfo.io/?token=' + TOKEN)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                if (data && data.loc) {
                    var locs = data.loc.split(',');
                    var loc = {
                        lat: locs[0],
                        lng: locs[1]
                    }
                    if (me.mapRef.current) me.mapRef.current.onLocationChange(loc);
                    me.setState({ location: loc, textSearch: data.city });
                }
            });
        me.setState(state => ({
            _isMounted: !state._isMounted
        }))
    }

    search = (e) => {
        e.preventDefault();
        let textSearch = this.state.textSearch;
        let me = this;
        if (textSearch.length > 2) {
            mapapi.algoliaGeocode(textSearch).then(function (result) {
                let loc = result._geoloc;
                if (me.mapRef.current) {
                    me.mapRef.current.onLocationChange(loc);
                }
            })
        }
    }

    openDialogBaoGia = () => {
        if (this.state._isMounted) {
            this.setState(state => ({
                modal: !state.modal
            }))
        }
    }

    chooseHome = (value) => {
        this.setState({ valueTypeHome: value });
        if (value === "apartment") {
            this.setState({ textHome: "Chung cư" });
            document.querySelector('#apartment').classList.add('active');
            document.querySelector('#house').classList.remove('active');
        } else if (value === "house") {
            this.setState({ textHome: "Nhà đất" });
            document.querySelector('#apartment').classList.remove('active');
            document.querySelector('#house').classList.add('active');
        }
        if (this.mapRef.current) {
            this.mapRef.current.onHomValueChange(value);
        }
        if (this.state.tabIndex == '0') {
            if (this.postRef.current) {
                this.postRef.current.chooseHome(value);
            }
        } else {
            if (this.priceRef.current) {
                this.priceRef.current.chooseHome(value);
            }
        }
    }

    handleChange = (evt) => {
        this.setState({ textSearch: evt.target.value });
    }
    toggle(tab) {
        if (this.state.tabIndex !== tab) {
            this.setState({
                tabIndex: tab,
            });
        }
    }

    onCityChange = (obj) => {

        //call post on citychange
        var myInterVal1 = setInterval(() => {
            if (this.postRef && this.postRef.current) {
                clearInterval(myInterVal1)
                this.postRef && this.postRef.current.onCityChange(obj);
            }
        }, 3000);

        ////call price on citychange
        var myInterVal2 = setInterval(() => {
            if (this.priceRef && this.priceRef.current) {
                clearInterval(myInterVal2)
                this.priceRef && this.priceRef.current.onCityChange(obj);
            }
        }, 3000);

        if (this.state.city_id === obj.id) {

            var cityInfo = this.state.cityInfo;
            if (cityInfo.address && cityInfo.address !== '') {
                this.setState({
                    unitChange: "city",
                    address: cityInfo.address,
                    fromValueCC: cityInfo.fromValueCC,
                    toValueCC: cityInfo.toValueCC,
                    totalMeanCC: cityInfo.totalMeanCC,
                    fromValueND: cityInfo.fromValueND,
                    toValueND: cityInfo.toValueND,
                    totalMeanND: cityInfo.totalMeanND
                });
            }
        }
        else {
            if (obj.name && obj.name !== '') {
                var cityInfo = {
                    unitChange: "city",
                    address: obj.name,
                    fromValueCC: obj.fromValueCC + ' triệu',
                    toValueCC: obj.toValueCC + ' triệu',
                    totalMeanCC: obj.totalMeanCC + ' triệu',
                    fromValueND: obj.fromValueND + ' triệu',
                    toValueND: obj.toValueND + ' triệu',
                    totalMeanND: obj.totalMeanND + ' triệu'
                }
                this.setState({
                    unitChange: "city",
                    address: obj.name,
                    fromValueCC: obj.fromValueCC + ' triệu',
                    toValueCC: obj.toValueCC + ' triệu',
                    totalMeanCC: obj.totalMeanCC + ' triệu',
                    fromValueND: obj.fromValueND + ' triệu',
                    toValueND: obj.toValueND + ' triệu',
                    totalMeanND: obj.totalMeanND + ' triệu',
                    cityInfo: cityInfo
                })
            }
        }
    }

    onDistrictChange = (obj) => {
        if (this.state.tabIndex === '0') {
            if (this.postRef && this.postRef.current)
                this.postRef && this.postRef.current.onDistrictChange(obj);
        } else {
            if (this.priceRef && this.priceRef.current)
                this.priceRef && this.priceRef.current.onDistrictChange(obj);
        }
        if (this.state.district_id === obj.id) return false;
        this.setState({
            district_id: obj.id,
            address: obj.name,
            fromValueCC: obj.fromValueCC + ' triệu',
            toValueCC: obj.toValueCC + ' triệu',
            totalMeanCC: obj.totalMeanCC + ' triệu',
            fromValueND: obj.fromValueND + ' triệu',
            toValueND: obj.toValueND + ' triệu',
            totalMeanND: obj.totalMeanND + ' triệu'
        })
    }

    onWardChange = (obj) => {
        if (this.state.district_id === obj.id) return false;
        this.setState({
            address: obj.name,
            fromValueCC: obj.fromValueCC + ' triệu',
            toValueCC: obj.toValueCC + ' triệu',
            totalMeanCC: obj.totalMeanCC + ' triệu',
            fromValueND: obj.fromValueND + ' triệu',
            toValueND: obj.toValueND + ' triệu',
            totalMeanND: obj.totalMeanND + ' triệu'
        })
        if (this.state.tabIndex === '0') {
            if (this.postRef && this.postRef.current)
                this.postRef && this.postRef.current.onWardChange(obj);
        } else {
            if (this.priceRef && this.priceRef.current)
                this.priceRef && this.priceRef.current.onWardChange(obj);
        }

    }

    receivePrice = (obj) => {
        let arrImages = obj.images.toString().split(",");
        this.setState({ itemSelected: obj, listImageItemSelected: arrImages })
        this.openDialogBaoGia();
    }

    toggleSidebar = () => {
        this.setState({ showColLeft: !this.state.showColLeft, }, () => {
            this.onDragEnd();
        });
    }

    onDragEnd = () => {
        if (this.mapRef.current) {
            this.mapRef.current.resizeMap();
        }
    }

    locatePoint = (item) => {
        if (this.mapRef.current) {
            var loc = {
                lng: item.longitude,
                lat: item.latitude
            }
            this.mapRef.current.onLocatePoint(loc);
        }
    }
    render() {
        const gridlayout1 = { xs: 12, sm: 12, md: 12, lg: 3, xl: 3, };
        const gridlayout2 = { xs: 12, sm: 12, md: 12, lg: 6, xl: 6 };
        const gridlayout3 = { xs: 12, sm: 12, md: 12, lg: 3, xl: 3 };

        const isView = (this.state.valueTypeHome === "house" ? (this.state.pageCountHouse > 0 ? true : false) : ((this.state.pageCountDepartment > 0 ? true : false)))
        return (
            <div>
                <Row style={{ margin: "5px 0" }}>
                    <Col xs={3} style={{}}>
                        <Button color="link" className="navbar-toggler-icon" onC={this.toggleSidebar} size="lg">
                            {this.state.showColLeft && <i className='fa fa-arrow-left'></i>}
                            {!this.state.showColLeft && <i className='fa fa-arrow-right'></i>}
                        </Button>
                    </Col>
                    <Col xs={6} lg={{ span: 3, }}>
                        <InputGroup>
                            <Input ref={this.addressRef} bsSize="lg"
                                id='address-input'
                                value={this.state.textSearch} onChange={this.handleChange}
                                placeholder="Ví dụ: Số 1 Xuân Thuỷ, Cầu Giấy, Hà Nội"
                                style={{ border: "solid 2px #20a8d8" }}
                            ></Input>
                        </InputGroup>
                    </Col>
                    <Col xs={3} lg={3}>
                        <div style={{ padding: "0.75rem 1.25rem", width: "100%" }}><strong style={{ float: "left", width: "30%" }}>Vị trí:</strong><p id="txtAddress" style={{ fontWeight: "bold", float: "left", color: "#20a8d8" }}></p></div>
                    </Col>
                </Row>
                <div className="home-container">
                    <Row>
                        <SplitterLayout onDragEnd={this.onDragEnd}
                            showColLeft={this.state.showColLeft}
                            primaryMinSize={500}
                            secondaryMinSize={500}
                        >
                            <div className="home-col-left" id="typeHome">
                                <Card style={{ marginBottom: "0px" }}>
                                    <CardBody style={{ paddingBottom: "5px" }}>
                                        <Row style={{ height: "15px" }}>
                                            <Col>
                                                <h4 style={{ textAlign: "center", lineHeight: "0px" }}>Địa chỉ: {this.state.address}</h4>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={6} id='apartment' className='house active' onClick={() => this.chooseHome('apartment')} >
                                                <Row>
                                                    <Col xs={4} className='text-center'>
                                                        <div style={{ fontSize: '50px' }}>
                                                            <i className='fa fa-building'></i>
                                                        </div>
                                                        <strong>Chung cư</strong>
                                                    </Col>
                                                    <Col xs={8}>
                                                        <p>Giá m2 trung bình</p>
                                                        <h5>{this.state.totalMeanCC}</h5>
                                                        <span>Từ {this.state.fromValueCC} đến {this.state.toValueCC}</span>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col sm={6} id='house' className='house' onClick={() => this.chooseHome('house')}>
                                                <Row>
                                                    <Col xs={4} className='text-center'>
                                                        <div style={{ fontSize: '50px' }}>
                                                            <i className='fa fa-home'></i>
                                                        </div>
                                                        <strong>Nhà đất</strong>
                                                    </Col>
                                                    <Col xs={8}>
                                                        <p>Giá m2 trung bình</p>
                                                        <h5>{this.state.totalMeanND}</h5>
                                                        <span>Từ {this.state.fromValueND} đến {this.state.toValueND}</span>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </CardBody>
                                </Card>
                                <Nav tabs>
                                    <NavItem>
                                        <NavLink className={classNames({ active: this.state.tabIndex === '0' })}
                                            onClick={() => {
                                                this.toggle('0');
                                            }}>
                                            <i className="fa fa-newspaper-o fa-lg">&nbsp;&nbsp;&nbsp;Bài viết</i>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className={classNames({ active: this.state.tabIndex === '1' })}
                                            onClick={() => {
                                                this.toggle('1');
                                            }}>
                                            <i className="fa fa-money fa-lg">&nbsp;&nbsp;&nbsp;Thông tin giá</i>
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <TabContent activeTab={this.state.tabIndex}>
                                    <TabPane tabId="0" style={{ padding: "0.25rem" }}>
                                        <Posts ref={this.postRef} mapRef={this.mapRef} > </Posts>
                                    </TabPane>
                                    <TabPane tabId="1" style={{ padding: "0.25rem" }}>
                                        <Price ref={this.priceRef} mapRef={this.mapRef} > </Price>
                                    </TabPane>
                                </TabContent>

                            </div>
                            <div sm="12" className="my-pane" id='col-map'>
                                <Map ref={this.mapRef} type={this.state.valueTypeHome} cityChange={this.onCityChange} districtChange={this.onDistrictChange} wardChange={this.onWardChange} />
                            </div>
                        </SplitterLayout>
                    </Row>
                </div>
            </div >
        );
    }
}
export default TrangChu;
