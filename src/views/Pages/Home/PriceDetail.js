import React from 'react';
import { Table, Media, Progress, Row, Col, ButtonGroup, Button, Nav, NavLink, NavItem, TabPane, TabContent, Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import { Line } from 'react-chartjs-2';
import { Link } from 'react-router-dom';
import houseApi from '../../../api/house.api';
import apartmentApi from '../../../api/appartment.api';
import mapApi from '../../../api/map.api';
import './Price.css';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';

const options = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips,
    },
    maintainAspectRatio: false,
    legend: {
        display: false,
    },
    elements: {
        point: {
            radius: 0,
            hitRadius: 10,
            hoverRadius: 4,
            hoverBorderWidth: 3,
        },
    },

}
const datasetDefault = {
    label: 'Price',
    backgroundColor: 'rgba(75,192,192,0.2)',
    borderColor: '#63c2de',
    pointHoverBackgroundColor: '#fff',
    borderWidth: 2,
};

class PriceDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            areaType: this.props.areaType,
            bdsType: this.props.bdsType,
            activeTab: new Array(4).fill('1'),
            showChart: false,
            showModal: false,
            showTable: true,
            location: {},
            data: [],
            dataChartHouse: [],
            dataChartAppartment: [],
            chartOf: 'house',
            radioSelected: 1,
            lineChart: {
                labels: [],
                datasets: [{
                    ...datasetDefault,
                    data: [],
                },
                ]
            },
            datatableHouse: [],
            datatableApartment: [],
            datatable: [],
            tableOf: 'house',
        };
        this.rScroll = React.createRef();
    }
    componentWillUpdate(e) {
        if (e.id != this.props.id) {
            console.log("idddd:", e.id);
            if (e.type === 'district') {
                this.loadDataDistrict(e.id);
            }
            else {
                this.loadDataWard(e.id);
            }
        }
        if(e.bdsType !== this.props.bdsType){
            // this.loadChart(e.bdsType, this.state.radioSelected)
            this.changeBdsType(e.bdsType);
        }
    }
    componentDidMount() {
        this.rScroll.current.style.height = '100%';
        let x = window.location.pathname;
        let arr = x.split('/');
        const id = arr[arr.length - 1];
        const type = arr[arr.length - 2];
        console.log(id);
        if (type === 'district') {
            this.loadDataDistrict(id);
        }
        else {
            this.loadDataWard(id);
        }
    }

    loadDataDistrict = (id) => {
        var promises = [];
        var getPriceHouse = new Promise((resolve, reject) => {
            houseApi.getPriceByDistrict(id).then(function (response) {
                if (response && response.length) {
                    resolve(response)
                } else
                    resolve([]);
            })
        })
        promises.push(getPriceHouse);
        var getPirceApartment = new Promise((resolve, reject) => {
            apartmentApi.getPriceByDistrict(id).then(function (response) {
                if (response && response.length) {
                    resolve(response);
                }
                else
                    resolve([]);
            })
        })
        promises.push(getPirceApartment);
        var getWardHouse = new Promise((resolve, reject) => {
            mapApi.getWardHouseStyle(id).then(function (response) {
                if (response && response.length) {
                    resolve(response);
                }
                else
                    resolve([]);
            })
        })
        promises.push(getWardHouse);
        var getWardApartment = new Promise((resolve, reject) => {
            mapApi.getWardAppartmentStyle(id).then(function (response) {
                if (response && response.length) {
                    resolve(response);
                }
                else
                    resolve([]);
            })
        })
        promises.push(getWardApartment);
        Promise.all(promises).then(values => {
            console.log("promises: ", values);
            this.setState({
                dataChartHouse: values[0],
                dataChartAppartment: values[1],
                datatableHouse: values[2],
                datatableApartment: values[3],
                id: id
            });
            this.loadChart(this.state.bdsType, this.state.radioSelected);
            this.loadTable(this.state.bdsType);
        })
    }
    loadDataWard = (id) => {
        var promises = [];
        var getPriceHouse = new Promise((resolve, reject) => {
            houseApi.getPriceByWard(id).then(function (response) {
                if (response && response.length) {
                    resolve(response)
                } else
                    resolve([]);
            })
        })
        promises.push(getPriceHouse);
        var getPirceApartment = new Promise((resolve, reject) => {
            apartmentApi.getPriceByWard(id).then(function (response) {
                if (response && response.length) {
                    resolve(response);
                }
                else
                    resolve([]);
            })
        })
        promises.push(getPirceApartment);

        Promise.all(promises).then(values => {
            this.setState({
                dataChartHouse: values[0],
                dataChartAppartment: values[1],
                datatableHouse: [],
                datatableApartment: [],
                datatable: [],
                id: id
            });
            this.loadChart(this.state.bdsType, this.state.radioSelected);
        })
    }
    changeChartOf(val) {
        this.loadChart(val, this.state.radioSelected);
    }
    onRadioBtnClick(radioSelected) {
        this.loadChart(this.state.bdsType, radioSelected);
    }
    loadChart(chartOf, radioSelected) {
        let data;
        if (chartOf === 'house') data = this.state.dataChartHouse;
        else data = this.state.dataChartAppartment;

        let temp;
        let arrLabel = [];
        let arrData = [];
        switch (radioSelected) {
            case 1: {
                temp = data[0].price_filter_by_time.day;
                for (var i of temp) {
                    arrLabel.push(i.date_str);
                    arrData.push(i.mean_ppm2.toFixed(1));
                }
                break;
            }
            case 2: {
                temp = data[0].price_filter_by_time.week;
                for (var i of temp) {
                    arrLabel.push(i.week_in_year.toString());
                    arrData.push(i.mean_ppm2.toFixed(1));
                }
                break;
            }
            case 3: {
                temp = data[0].price_filter_by_time.month;
                for (var i of temp) {
                    arrLabel.push(i.month_str);
                    arrData.push(i.mean_ppm2.toFixed(1));
                }
                break;
            }
            default: {
                temp = data[0].price_filter_by_time.year;
                for (var i of temp) {
                    arrLabel.push(i.year_str);
                    arrData.push(i.mean_ppm2.toFixed(1));
                }
            }
        }

        let datasetTemp = {
            ...datasetDefault,
            data: arrData,
        }
        this.setState(prevState => ({
            lineChart: {
                ...prevState.lineChart,
                labels: arrLabel,
                datasets: [datasetTemp],
            },
            radioSelected: radioSelected,
            bdsType: chartOf,
        }));

    }

    changeBdsType(val) {
        this.loadChart(val, this.state.radioSelected);
        this.loadTable(val);
    }
    loadTable(e) {
        if (e === 'house') {
            this.setState({ datatable: this.state.datatableHouse })
        }
        else {
            this.setState({ datatable: this.state.datatableApartment })
        }
    }
    render() {

        return (
            <div>
                <div id='scroll-to-top' ref={this.rScroll}>
                    <div>
                        <Row>
                            <Col className='text-right'>
                                <div>
                                    <ButtonGroup className="mr-3" aria-label="First group">
                                        <Button color="outline-primary"
                                            onClick={() => this.onRadioBtnClick(1)}
                                            active={this.state.radioSelected === 1}
                                        >
                                            Ngày
                                                                </Button>
                                        <Button color="outline-primary"
                                            onClick={() => this.onRadioBtnClick(2)}
                                            active={this.state.radioSelected === 2}
                                        >
                                            Tuần
                                                                </Button>
                                        <Button color="outline-primary"
                                            onClick={() => this.onRadioBtnClick(3)}
                                            active={this.state.radioSelected === 3}
                                        >
                                            Tháng
                                                                </Button>
                                        <Button color="outline-primary"
                                            onClick={() => this.onRadioBtnClick(4)}
                                            active={this.state.radioSelected === 4}
                                        >
                                            Năm
                                                                </Button>
                                    </ButtonGroup>

                                </div>
                            </Col>
                        </Row>
                        <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                            <Line data={this.state.lineChart} options={options} height={300} />
                        </div>
                    </div>
                    <br/>

                    {/* <div className='price-table-container'> */}
                    {this.state.datatable.length > 0
                        &&
                        <div>
                            {/* Bảng giá */}
                            <Table>
                                <thead>
                                    <tr>
                                        <th>Khu vực</th>
                                        <th>Giá thấp nhất</th>
                                        <th>Giá cao nhất</th>
                                        <th>Giá trung bình</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.datatable.map(i => (
                                        <tr key={i.id}>
                                            <td><Link to={'/home/price/ward/' + i.id}>{i.name}</Link></td>
                                            <td>{i.min.toFixed(1)}</td>
                                            <td>{i.max.toFixed(1)}</td>
                                            <td>{i.ppm2_mean.toFixed(1)}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>

                        </div>
                    }
                </div>
            </div>
        )
    }
}
PriceDetail.defaultProps = {
    bdsType: 'house',
    areaType: 'district',
}
export default PriceDetail;