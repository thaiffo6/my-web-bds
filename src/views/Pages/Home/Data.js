export const dataHome = [
    {
        id: "da1",
        state: 'ĐÃ BÁN',
        imageUrl: 'apartment.jpg',
        description1: 'Một căn hộ đẹp và sang trọng đáp ứng đủ nhu cầu về cuộc sống hàng ngày của bạn ',
        description2: 'Bán vào tháng 6 năm 2019',
        address: "da1",
        type: 'apartment'
    },
    {
        id: "da2",
        state: 'ĐÃ BÁN',
        imageUrl: 'apartment.jpg',
        description1: 'Một căn hộ đẹp và sang trọng đáp ứng đủ nhu cầu về cuộc sống hàng ngày của bạn ',
        description2: 'Bán vào tháng 7 năm 2019',
        address: "da2",
        type: 'apartment'
    },
    {
        id: "da3",
        state: '',
        imageUrl: 'apartment.jpg',
        description1: 'Một căn hộ đẹp và sang trọng đáp ứng đủ nhu cầu về cuộc sống hàng ngày của bạn ',
        description2: 'Bán vào tháng 8 năm 2019',
        address: "da3",
        type: 'apartment'
    },
    {
        id: "da4",
        state: '',
        imageUrl: 'apartment.jpg',
        description1: 'Một căn hộ đẹp và sang trọng đáp ứng đủ nhu cầu về cuộc sống hàng ngày của bạn ',
        description2: 'Bán vào tháng 9 năm 2019',
        address: "da4",
        type: 'apartment'
    },
    {
        id: "da5",
        state: '',
        imageUrl: 'apartment.jpg',
        description1: 'Một căn hộ đẹp và sang trọng đáp ứng đủ nhu cầu về cuộc sống hàng ngày của bạn ',
        description2: 'Bán vào tháng 9 năm 2019',
        address: "da5",
        type: 'apartment'
    },
    {
        id: "dh1",
        state: '',
        imageUrl: 'housing.jpg',
        description1: 'Ngôi nhà với những tiện ích lí tưởng đã sẵn sàng chào đón bạn làm chủ nhân của nó',
        description2: 'Bán vào tháng 6 năm 2019',
        address: "dh1",
        type: 'housing'
    },
    {
        id: "dh2",
        state: '',
        imageUrl: 'housing.jpg',
        description1: 'Ngôi nhà với những tiện ích lí tưởng đã sẵn sàng chào đón bạn làm chủ nhân của nó',
        description2: 'Bán vào tháng 7 năm 2019',
        address: "dh2",
        type: 'housing'
    },
    {
        id: "dh3",
        state: '',
        imageUrl: 'housing.jpg',
        description1: 'Ngôi nhà với những tiện ích lí tưởng đã sẵn sàng chào đón bạn làm chủ nhân của nó',
        description2: 'Bán vào tháng 8 năm 2019',
        address: "dh3",
        type: 'housing'
    },
    {
        id: "dh4",
        state: '',
        imageUrl: 'housing.jpg',
        description1: 'Ngôi nhà với những tiện ích lí tưởng đã sẵn sàng chào đón bạn làm chủ nhân của nó',
        description2: 'Bán vào tháng 9 năm 2019',
        address: "dh4",
        type: 'housing'
    },
    {
        id: "dh5",
        state: '',
        imageUrl: 'housing.jpg',
        description1: 'Ngôi nhà với những tiện ích lí tưởng đã sẵn sàng chào đón bạn làm chủ nhân của nó',
        description2: 'Bán vào tháng 9 năm 2019',
        address: "dh4",
        type: 'housing'
    },
]

export const listImage = [
    {
        src: 'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa1d%20text%20%7B%20fill%3A%23555%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa1d%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22285.921875%22%20y%3D%22218.3%22%3EFirst%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E',
        altText: 'Slide 1',
        caption: 'Slide 1',
        header: 'Slide 1 Header',
        key: '1'
      },
      {
        src: 'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa20%20text%20%7B%20fill%3A%23444%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa20%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23666%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22247.3203125%22%20y%3D%22218.3%22%3ESecond%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E',
        altText: 'Slide 2',
        caption: 'Slide 2',
        header: 'Slide 2 Header',
        key: '2'
      },
      {
        src: 'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa21%20text%20%7B%20fill%3A%23333%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa21%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23555%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22277%22%20y%3D%22218.3%22%3EThird%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E',
        altText: 'Slide 3',
        caption: 'Slide 3',
        header: 'Slide 3 Header',
        key: '3'
      }
];

export const listTypeHouse = [
        {
            id: 1,
            value: 'apartment',
            name: 'Nhà chung cư'
        },
        {
            id: 2,
            value: 'housing',
            name: 'Nhà đất'
        },
];
