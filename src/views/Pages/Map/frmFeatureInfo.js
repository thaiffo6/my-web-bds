import React, { Component } from "react";
// import { AppContext } from "./../context";
import {
  Button,
  Col,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Label,
  Row,
  Table
} from "reactstrap";
import ModalDraggable from "./../../Comps/ModalDraggable";

class FormFeatureInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      feaData: {}
    };
  }

  toggleDuongPho = () => {
    // this.context.duongPhoToggle2();
    this.setState({ duongPho: !this.state.duongPho });
  };

  toggleToDanPho = () => {
    // this.context.toDanPhoToggle2();
    this.setState({ toDanPho: !this.state.toDanPho });
  };
  formatHtml(data) {
    let a_objTd = data.thuadat;
    if (a_objTd.length == 0) {
      console.log("Không có thông tin thửa đất");
      return "<p>Không có thông tin thửa đất!</p>";
    }
    let a_objMdsd = data.mdsd;
    let a_objCsh = data.csh;
    let a_objGcn = data.gcn;
    var tdHtml =
      `<div style="background-color:white;padding-left:10px;"><span style="box-shadow:none;font-weight:bold;box-shadow:none;">Thửa đất</span><ul style="list-style:none;">
          <li><span style="box-shadow:none;">Số tờ: ` +
      a_objTd[0]["SOTO"] +
      `</span></li>
          <li><span style="box-shadow:none;">Số thửa: ` +
      a_objTd[0]["SOTHUA"] +
      `</span></li>
          <li><span style="box-shadow:none;">Địa chỉ: ` +
      a_objTd[0]["DIACHI"] +
      `</span></li>
          <li><span style="box-shadow:none;">Số tờ cũ: ` +
      a_objTd[0]["SOTOCU"] +
      `</span></li>
          <li><span style="box-shadow:none;">Số thửa cũ: ` +
      a_objTd[0]["SOTHUACU"] +
      `</span></li>
       </ul></div>`;
    var mdsdHtml =
      '<div style="background-color:white;padding-left:10px;"><span style="box-shadow:none;font-weight:bold;background-color:white;">Mục đích sử dụng</span><ul style="background-color:white;list-style:none;">';
    for (var i = 0; i < a_objMdsd.length; i++) {
      var mdmct = a_objMdsd[i];
      for (var field in mdmct) {
        if (mdmct[field] == undefined) {
          mdmct[field] = "Không có";
        }
      }
      mdsdHtml +=
        '<li><span style="box-shadow:none;">' +
        mdmct["MDSD"] +
        ": " +
        mdmct["DIENTICH"] +
        " m2</span></li>";
    }
    mdsdHtml += "</ul></div>";
    var cshHtml =
      '<div style="background-color:white;padding-left:10px;"><span style="box-shadow:none;font-weight:bold;background-color:white;">Chủ sở hữu</span><ul style="background-color: white;list-style:none;">';
    for (var i = 0; i < a_objCsh.length; i++) {
      var cshct = a_objCsh[i];
      for (var field in cshct) {
        if (cshct[field] == undefined) {
          cshct[field] = "Không có";
        }
      }
      cshHtml +=
        '<li><span style="box-shadow:none;">' +
        cshct["DANHXUNG"] +
        ": " +
        cshct["CSHTEN"] +
        "</span></li>";
      cshHtml +=
        '<li><span style="box-shadow:none;">Số giấy tờ: ' +
        cshct["CSHSOGT"] +
        "</span></li>";
      cshHtml +=
        '<li><span style="box-shadow:none;">Loại giấy tờ: ' +
        cshct["CSHLOAIGT"] +
        "</span></li>";
      cshHtml +=
        '<li><span style="box-shadow:none;">Địa chỉ: ' +
        cshct["CSHDIACHI"] +
        "</span></li>";
    }
    cshHtml += "</ul></div>";
    var gcnHtml =
      '<div style="background-color:white;padding-left:10px;"><span style="box-shadow:none;font-weight:bold;background-color:white;">Giấy chứng nhận</span><ul style="background-color:white;list-style:none;">';
    for (var i = 0; i < a_objGcn.length; i++) {
      var gcnct = a_objGcn[i];
      for (var field in gcnct) {
        if (gcnct[field] == undefined) {
          gcnct[field] = "Không có";
        }
      }
      gcnHtml +=
        '<li><span style="box-shadow:none;">Số vào sổ: ' +
        gcnct["SOVS"] +
        "</span></li>";
      gcnHtml +=
        '<li><span style="box-shadow:none;">Số hiệu GCN: ' +
        gcnct["SOHIEUGCN"] +
        "</span></li>";
      gcnHtml +=
        '<li><span style="box-shadow:none;">Ngày cấp: ' +
        gcnct["NGAYCAP"] +
        "</span></li>";
      gcnHtml +=
        '<li><span style="box-shadow:none;">Chính thức có pháp lý: ' +
        gcnct["COPL"] +
        "</span></li>";
      //gcnHtml += '<li><span style="box-shadow:none;">Bản quét: ' + gcnct['BANQUET'] + '</span></li>';
      gcnHtml +=
        '<li><span style="box-shadow:none;">Bản quét: <a href="#" onclick="return document.getElementById(\'ibd_view\').contentWindow.viewFiles(\'' +
        gcnct["SOHIEUGCN"] +
        "');\">" +
        gcnct["SOHIEUGCN"] +
        "</a></span></li>";
    }
    gcnHtml += "</ul></div>";
    var height = window.innerHeight - 130;
    var html =
      '<div style="height:' +
      height +
      'px;">' +
      tdHtml +
      mdsdHtml +
      cshHtml +
      gcnHtml +
      "</div>";
    return html;
  }
  render() {
    const gridlayout = { xs: 12, sm: 6 };
    const gridlabel = { xs: 12, sm: 12, md: 4 };
    const gridinput = { xs: 12, sm: 12, md: 8 };
    const gridlabel2 = { xs: 12, sm: 12, md: 2 };
    const gridinput2 = { xs: 12, sm: 12, md: 10 };
    const featureInfo = this.props.FeatureInfo;
    const feaHtml = this.formatHtml(featureInfo);
    return (
      <div className="animated fadeIn">
        <div
          dangerouslySetInnerHTML={{
            __html: feaHtml.replace(/(<? *script)/gi, "illegalscript")
          }}
        ></div>
      </div>
    );
  }
}
FormFeatureInfo.defaultProps = {
  FeatureInfo: {}
};
// FormFeatureInfo.contextType = AppContext;
export default FormFeatureInfo;
