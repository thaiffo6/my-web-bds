import { ApiClient } from "./api";
const api = new ApiClient();
export default {
  //danh sách nhà đất theo thành phố
  async getByCity(id, page, limit) {
    let url = "/records-house-city?city_id=" + id + "&page_number=" + page + "&limit=" + limit;
    const result = await api.get(url);
    return result;
  },
  //danh sách nhà đất theo quận
  async getByDistrict(id, page, limit) {
    let url = "/records-house-district?district_id=" + id + "&page_number=" + page + "&limit=" + limit;
    const result = await api.get(url);
    return result;
  },
  //danh sách nhà đất theo xã
  async getByWard(id, page, limit) {
    let url = "/records-house-ward?ward_id=" + id + "&page_number=" + page + "&limit=" + limit;
    const result = await api.get(url);
    return result;
  },

  //Lấy thông tin giá theo quận cho biểu đồ
  async getPriceByDistrict(id) {
    let url = "/district-house-graph?district_id=" + id;
    const result = await api.get(url);
    return result;
  },
  //Lấy thông tin giá theo phường cho biểu đồ
  async getPriceByWard(id) {
    let url = "/ward-house-graph?ward_id=" + id;
    const result = await api.get(url);
    return result;
  }
}