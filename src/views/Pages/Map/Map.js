import React, { Component } from 'react';
import {
    Badge,
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupButtonDropdown,
    InputGroupText,
    Label,
    Row,
    Table,
    Pagination,
    PaginationItem,
    PaginationLink
} from 'reactstrap';
import MapComponent from './index';

class Map extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300
        };
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState } });
    }

    render() {
        const gridlayout = {
            xs: 12, sm: 12, lg: 6
        };
        const gridlabel = {
            xs: 12, sm: 3, lg: 4
        }
        const gridinput = {
            xs: 12, sm: 9, lg: 8
        }

        return (
            <div className="animated fadeIn">
                <div>
                    <MapComponent />
                </div>
            </div>
        );
    }
}

export default Map;