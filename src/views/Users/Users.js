import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table, FormGroup, Input, Label, Button, Pagination, PaginationItem, PaginationLink, InputGroup, InputGroupAddon } from 'reactstrap';

import usersData from './UsersData'
usersData.reverse();

function UserRow(props) {
  const user = props.user;
  const userLink = `/users/${user.id}`;

  const getBadge = (status) => {
    return status === 'Active' ? 'success' :
      status === 'Inactive' ? 'secondary' :
        status === 'Pending' ? 'warning' :
          status === 'Banned' ? 'danger' :
            'primary'
  }
  const clickRow = e => {
    if (props.callback !== undefined) props.callback(e);
  }
  return (
    <tr key={user.id.toString()} onClick={() => { clickRow(props.user) }}>
      <th scope="row">{user.id}</th>
      <td><Link to={userLink}>{user.name}</Link></td>
      <td>{user.registered}</td>
      <td>{user.role}</td>
      <td><Badge color={getBadge(user.status)}>{user.status}</Badge></td>
    </tr>
  )
}

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      registered: '',
      name: '',
      role: '',
      status: '',
      data: usersData,
      current: 1,
      listPage: [1],
      numRow: 8,
      keyword: ''
    };
  }

  changePage = e => {
    this.setState({ current: e });
  }

  handleAdd = () => {
    var dt = new Date();
    var month = dt.getMonth() + 1;
    if (month < 10) month = "0" + month;
    var day = dt.getDate();
    if (day < 10) day = "0" + day;

    var user = {
      id: usersData.length,
      registered: dt.getFullYear() + "-" + month + "-" + day,
      name: this.state.name,
      role: this.state.role,
      status: this.state.status
    };
    usersData.unshift(user);
    this.setState({
      data: usersData,
      id: '',
      registered: '',
      name: '',
      role: '',
      status: ''
    });
  }

  handleUpdate = () => {
    if (this.state.id === '') {
      alert("ERROR: Chưa chọn user cần update!")
      return;
    }
    for (var i of usersData) {
      if (i.id === this.state.id) {
        i.name = this.state.name;
        i.role = this.state.role;
        i.status = this.state.status;
        break;
      }
    }
    this.setState({
      data: usersData,
      id: '',
      registered: '',
      name: '',
      role: '',
      status: ''
    });
  }
  handleDelete = () => {
    if (this.state.id === '') {
      alert("ERROR: Chưa chọn user cần xóa!");
      return;
    }
    var i = 0;
    for (i; i < usersData.length; i++) {
      if (usersData[i].id === this.state.id) break;
    }
    usersData.splice(i, 1);
    this.setState({
      data: usersData,
      id: '',
      registered: '',
      name: '',
      role: '',
      status: ''
    });
  }
  handleClickRow = e => {
    this.setState({
      id: e.id,
      registered: e.registered.replace(/\//g, '-'),
      name: e.name,
      role: e.role,
      status: e.status
    })
  }
  handleSearch = () => {
    let keyword = this.state.keyword.toLowerCase();
    let datatemp = usersData;
    let data = datatemp.filter((i) => (
      keyword === i.id || i.name.toLowerCase().indexOf(keyword) !== -1
    ));
    this.setState({ data: data, current: 1 });

  }
  handleRefresh = () => {
    this.setState({ keyword: '', data: usersData, current: 1 });
  }
  // handle Onchange
  changeId = i => {
    this.setState({ id: i.target.value });
  }
  changeName = i => {
    this.setState({ name: i.target.value });
  }
  changeRole = i => {
    this.setState({ role: i.target.value });
  }
  changeStatus = i => {
    this.setState({ status: i.target.value });
  }
  changeKeyword = i => {
    this.setState({ keyword: i.target.value });
  }
  render() {
    // get UserList
    let start = (this.state.current - 1) * this.state.numRow;
    const userList = this.state.data.slice(start, start + this.state.numRow);
    //  get List Page
    let end = Math.ceil(this.state.data.length / this.state.numRow);
    let listPage = [];
    for (let i = 1; i <= end; i++) listPage.push(i);
    //layout
    const gridlayout = { xs: 12 };
    const gridlabel = { xs: 12, sm: 2, md: 12, lg: 3 };
    const gridinput = { xs: 12, sm: 10, md: 12, lg: 9 };

    return (
      <div className="animated fadeIn">
        <Row>
          <Col md={8}>
            <Card>
              <CardHeader>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'baseline' }}>
                  <div>
                    <i className="fa fa-align-justify"></i> User List
                  </div>
                  <div>
                    <InputGroup>
                      <Input type='text' placeholder='Nhập ID hoặc Name' value={this.state.keyword} onChange={this.changeKeyword} />
                      <InputGroupAddon addonType='prepend'>
                        <Button color='primary' onClick={this.handleSearch}><i className='fa fa-search'></i></Button>
                        <Button color='danger' onClick={this.handleRefresh}><i className='fa fa-refresh'></i></Button>
                      </InputGroupAddon>
                    </InputGroup>
                  </div>
                </div>
              </CardHeader>
              <CardBody>
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th scope="col">id</th>
                      <th scope="col">name</th>
                      <th scope="col">registered</th>
                      <th scope="col">role</th>
                      <th scope="col">status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {userList.map((user, index) =>
                      <UserRow key={index} user={user} callback={this.handleClickRow} />
                    )}
                  </tbody>
                </Table>
                <Pagination>
                  {listPage.map(i => (
                    i === this.state.current
                      ?
                      <PaginationItem key={i} active>
                        <PaginationLink tag="button">{i}</PaginationLink>
                      </PaginationItem>
                      :
                      <PaginationItem key={i} onClick={() => { this.changePage(i) }}>
                        <PaginationLink tag="button">{i}</PaginationLink>
                      </PaginationItem>
                  ))}
                </Pagination>
              </CardBody>
            </Card>
          </Col>

          <Col md={4}>
            <Card>
              <CardHeader><i className='fa fa-user-circle'></i> Management</CardHeader>
              <CardBody>
                <Row>
                  <Col {...gridlayout}>
                    <FormGroup row>
                      <Col {...gridlabel}>
                        <Label>ID</Label>
                      </Col>
                      <Col {...gridinput}>
                        <Input type='text' value={this.state.id} readOnly />
                      </Col>
                    </FormGroup>
                  </Col>
                  <Col {...gridlayout}>
                    <FormGroup row>
                      <Col {...gridlabel}>
                        <Label>Registered</Label>
                      </Col>
                      <Col {...gridinput}>
                        <Input type='date' value={this.state.registered} readOnly />
                      </Col>
                    </FormGroup>
                  </Col>
                  <Col {...gridlayout}>
                    <FormGroup row>
                      <Col {...gridlabel}>
                        <Label>Name</Label>
                      </Col>
                      <Col {...gridinput}>
                        <Input type='text' value={this.state.name} onChange={this.changeName} />
                      </Col>
                    </FormGroup>
                  </Col>
                  <Col {...gridlayout}>
                    <FormGroup row>
                      <Col {...gridlabel}>
                        <Label>Role</Label>
                      </Col>
                      <Col {...gridinput}>
                        <Input type='select' value={this.state.role} onChange={this.changeRole}>
                          <option value='' hidden>Select Role</option>
                          <option value='Guest'>Guest</option>
                          <option value='Member'>Member</option>
                          <option value='Staff'>Staff</option>
                          <option value='Admin'>Admin</option>
                        </Input>
                      </Col>
                    </FormGroup>
                  </Col>
                  <Col {...gridlayout}>
                    <FormGroup row>
                      <Col {...gridlabel}>
                        <Label>Status</Label>
                      </Col>
                      <Col {...gridinput}>
                        <Input type='select' value={this.state.status} onChange={this.changeStatus}>
                          <option value='' hidden>Select Status</option>
                          <option value='Pending'>Pending</option>
                          <option value='Active'>Active</option>
                          <option value='Inactive'>Inactive</option>
                          <option value='Banned'>Banned</option>
                        </Input>
                      </Col>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormGroup row>
                      <Col {...gridlabel}></Col>
                      <Col {...gridinput}>
                        <Button color='success' onClick={this.handleAdd}><i className='fa fa-fresh'></i>Add</Button>{' '}
                        <Button color='warning' onClick={this.handleUpdate}><i className='fa fa-fresh'></i>Update</Button>{' '}
                        <Button color='danger' onClick={this.handleDelete}><i className='fa fa-fresh'></i>Delete</Button>
                      </Col>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Users;
