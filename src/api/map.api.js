import axios from "axios";
import { async } from "q";
import { ApiClient } from "./api";
import AppConfig from "./../AppConfig";
const google_apikey = 'AIzaSyC_9uIU4k81jSfNb8LISj1EhKOoerfKhe8';
const algolia_appid = 'pl0K4C57LHC4';
const algolia_apikey = '74d8852573f9c6ff69b6e9e87f05efb6';

//import { async } from "q";
const api = new ApiClient();
export default {
    //Tìm theo địa chỉ
    async algoliaGeocode(address) {
        let url = 'https://places-dsn.algolia.net/1/places/query';
        let data = { query: address }
        return new Promise((resolve, reject) => {
            axios.post(url, data, {
                headers: {
                    'X-Algolia-Application-Id': algolia_appid,
                    'X-Algolia-API-Key': algolia_apikey
                }
            }).then(function (response) {
                if (response.status === 200) {
                    let data = response.data;
                    if (data.hits.length > 0) {
                        resolve(data.hits[data.hits.length - 1]);
                    }
                } else {
                    reject();
                }
            });
        })
    },
    async googleGeoCode(address) {
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + google_apikey;
        const result = await axios.get(url);
        return result;
    },
    async revertGeoCode(address) {
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + google_apikey;
        const result = await axios.get(url);
        return result;
    },
    async getDistrictHouseStyle(id) {
        let url = "/districts-house-city-map?city_id=" + id;
        const result = await api.get(url);
        return result;
    },
    async getDistrictAppartmentStyle(id) {
        let url = "/districts-apartment-city-map?city_id=" + id;
        const result = await api.get(url);
        return result;
    },
    async getWardHouseStyle(id) {
        let url = "/wards-house-district-map?district_id=" + id;
        const result = await api.get(url);
        return result;
    },
    async getWardAppartmentStyle(id) {
        let url = "/wards-apartment-district-map?district_id=" + id;
        const result = await api.get(url);
        return result;
    },
    async getFeaturesById(layerid, id) {
        let url = AppConfig.MAPAPI + '/diaphan/' + layerid + '/features?id=' + id;
        const result = await axios.get(url);
        return result;
    }
};
