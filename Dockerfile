FROM node:12-alpine as build

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json

RUN npm config set http-proxy http://10.55.123.98:3128/
RUN npm config set https-proxy http://10.55.123.98:3128/
ENV proxy=http://10.55.123.98:3128

RUN npm install
COPY . /app

RUN npm run build
# production environment

FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
COPY --from=build /app/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 3000
CMD ["nginx", "-g", "daemon off;"]
